

public loaded: boolean;
private inspectionSubject = new BehaviorSubject<Inspection[]>([]);

loadInspections(page = 0,  nomeCliente?: string) {
    this.loadingService.start();

    this.service.page(page, this.pageSize, nomeCliente).pipe(
      finalize(() => {
        this.loaded = true;
        this.loadingService.stop();
      })
    ).subscribe(resp => {
      if (resp && resp.dados) {
        this.inspectionSubject.next(resp.dados.content);
        this.length = resp.dados.totalElements ? resp.dados.totalElements : 0;
      }
    });
  }
}

 getByName() {
    if (!this.nomeCliente) {
      this.messageService.sendMessage(
        'Preencha um nome para pesquisar.',
        'danger'
      );
      return;
    }
    this.datasource.loaded = false;
    this.datasource.loadInspections(0, this.nomeCliente);
    this.buscou = true;
  }

  clear() {
    this.buscou = false;
    this.nomeCliente = '';
    this.datasource.loaded = false;
    this.datasource.loadInspections(0, this.nomeCliente);
  }