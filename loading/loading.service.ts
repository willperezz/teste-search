import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class LoadingService {
  private loadingSubject = new BehaviorSubject<boolean>(false);

  constructor() { }

  public start() {
    this.loadingSubject.next(true);
  }

  public stop() {
    this.loadingSubject.next(false);
  }

  get isLoading(): Observable<any> {
    return this.loadingSubject.asObservable();
  }
}
