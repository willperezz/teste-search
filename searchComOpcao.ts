
 
 
 public async search() {
    if (this.searchValue) {
      if (this.searchField === 'cliente') {
        const searchClients = new SearchBody();
        searchClients.page = 0;
        searchClients.size = 100;
        searchClients.filters = new SearchFilter();
        searchClients.filters.and = [];
        searchClients.filters.and.push(SearchFilter.createPrefix('nome', this.searchValue));
        this.loadingService.start();
        await this.clientService.search(searchClients).toPromise().then(response => {
          console.log('depois da req de cliente');
          const clientes = response.content as Client[];
          this.searchBody.filters = new SearchFilter();
          this.searchBody.filters.or = [];
          clientes.forEach(cliente => {
            this.searchBody.filters.or.push(SearchFilter.createEqual('cliente', cliente.codigo));
          });
        }).finally(() => { this.loadingService.stop(); });
      } else if (this.searchField === 'tipo') {
        this.searchBody.filters = new SearchFilter();
        this.searchBody.filters.and = [];
        console.log('JSON.stringify(this.searchValue)', JSON.stringify(this.searchValue));
        this.searchBody.filters.and.push(SearchFilter.createEqualObject(this.searchField, JSON.stringify(this.searchValue)));
      } else {
        this.searchBody.filters = new SearchFilter();
        this.searchBody.filters.and = [];
        this.searchBody.filters.and.push(SearchFilter.createPrefix(this.searchField, this.searchValue));
      }
      console.log('depois do if');
      this.datasource.loaded = false;
      this.datasource.loadAnalysis(this.searchBody);
      this.buscou = true;
    } else {
      const field = this.fields.filter(f => f.name === this.searchField)[0].desc;
      this.messageService.sendMessage('Informe o ' + field + ' para pesquisar.', 'danger');
    }
  }

  public clear() {
    this.buscou = false;
    this.searchValue = undefined;
    this.createDeaultSearch();
    this.datasource.loaded = false;
    this.datasource.loadAnalysis(this.searchBody);
  }
